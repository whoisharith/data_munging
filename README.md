This project contains code to read and parse a text file containing tables (columns separated by spaces). The program finds the row in a data file with the minimum difference in values of two fields.

In weather.dat, the day with the minimum temperature spread across a period of 30 days is calculated, from a weather data table. 
    
    (Temp spread = Daily Max Temp - Daily Min Temp)

In football.dat, the team with the least difference in goals scored ('F') vs goals conceded ('A') is found from a league table.

The program is built while adhering to SRP (Single responsibility principle) and OECM (Open for extension, closed for modification) principles of Object-Oriented Programming.

If new types of data files are to be added, create new classes with methods which need to be overwritten for the new types of table. 

    football.dat contains Premier League table from 2001-02.
    weather.dat contains weather data for Morristown, NJ from June 2002.

Requirements:

    Python 3.7

Instructions:

Run this command from the project root directory:
    
    python main.py

Directory structure:

    data_munging
    ├── extract_analyse.py
    ├── football.dat
    ├── football.py
    ├── main.py
    ├── README.md
    ├── weather.dat
    └── weather.py