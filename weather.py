"""Contains DataExtractor class."""
from extract_analyse import DataExtractor

class WeatherDataExtractor(DataExtractor):
    """Inherits from DataExtractor and redefines read_rows() to adapt to
    a weather data table."""
    def read_rows(self):
        """Reads rows in a weather table data file."""
        self.file.seek(0)
        self.file.readline()
        self.row_list = []
        for each_day in self.file.readlines():
            todays_data = each_day.split()
            if len(todays_data) == 0:
                continue
            todays_data_dict = dict(zip(self.columns, todays_data))
            self.row_list.append(todays_data_dict)
