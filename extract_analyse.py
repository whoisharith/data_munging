"""Contains classes DataExtractor and DataAnalyser which contain methods used\
to read and analyse data from a text file, which is in the form of a table."""

import re


class DataExtractor():
    """Contains attributes and methods for DataExtractor."""
    def __init__(self, path):
        """Initialise the instance with the file path.\n
        :param path : filepath of .dat file"""
        self.path = path

    def open_file(self):
        """Read data inside file and store it as a file"""
        self.file = open(self.path, 'r')
        self.data = {}

    def read_column_names(self):
        """Read columns and store as a list."""
        self.file.seek(0)
        self.columns = self.file.readline().split()

    def read_rows(self):
        """Read each row in the data file and store as list of dictionaries."""
        self.file.seek(0)
        self.file.readline()
        self.row_list = []
        for row in self.file.readlines():
            split_row = row.split()
            if len(split_row) > 0:
                full_row = dict(zip(self.columns, split_row))
                self.row_list.append(full_row)

    def close_file(self):
        """Close the current file."""
        self.file.close()


class DataAnalyser():
    """Contains methods to process data from a .dat file containing tables."""
    def extract_and_analyse(self, filename, field_1, field_2):
        """Calls the required methods on instances of DataExtractor
        class/subclasses, and analyses the data.\n
        :param filename : filename on which to call DataExtractor methods.\n
        :param field_1 : string, name of first field.\n
        :param field_2 : string, name of second field.\n
        """
        filename.open_file()
        filename.read_column_names()
        filename.read_rows()
        self.get_selected_fields(filename, field_1, field_2)
        name, minimum = self.get_min_difference(filename, field_1, field_2)
        if minimum%1 == 0:
            minimum = int(minimum)
        filename.close_file()
        print("\nAnalysing data from file : ", filename.path)
        print("Row identifier : ", name, ", Value : ", minimum)

    def get_selected_fields(self, datafile, *fields):
        """Stores the values of passed fields from the data.\n
        :param datafile : instance of DataExtractor class, or
         any of its sub-classes.\n
        :param fields : tuple of field names, whose data are to be extracted.
        """
        fields = list(fields)
        fields.insert(0, datafile.columns[0])
        for field in fields:
            datafile.data[field] = []
            for each_row_dict in datafile.row_list:
                datafile.data[field].append(each_row_dict[field])

    def get_min_difference(self, datafile, field_1, field_2):
        """Finds minimum difference of two fields, and the row identifier
        in a data file.\n
        :param datafile : instance of DataExtractor class, or
         any of its sub-classes.\n
        :param field_1 : string, name of first field.\n
        :param field_2 : string, name of second field.\n
        :return min_name : string, identifier of row with minimum difference.\n
        :return min_diff : float, value of minimum difference.
        """
        min_diff, min_name = None, None
        for index, each_row in enumerate(datafile.row_list):
            num1_string = datafile.data[field_1][index]
            num2_string = datafile.data[field_2][index]
            num1 = float(re.sub(r'[^0-9.]', '', num1_string))
            num2 = float(re.sub(r'[^0-9.]', '', num2_string))
            #Remove characters other than digits and period.
            diff = abs(num1-num2)
            if min_diff is None or min_diff > diff:
                min_diff = diff
                min_name = each_row[datafile.columns[0]]
        return min_name, min_diff
