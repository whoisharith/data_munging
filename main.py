"""Run this file to execute the program.
"""
from football import FootballTableExtractor
from weather import WeatherDataExtractor
from extract_analyse import DataAnalyser


def main():
    """Main function."""
    analyser = DataAnalyser()

    football = FootballTableExtractor('football.dat')
    weather = WeatherDataExtractor('weather.dat')

    analyser.extract_and_analyse(football, 'F', 'A')
    analyser.extract_and_analyse(weather, 'MxT', 'MnT')


if __name__ == '__main__':
    main()
