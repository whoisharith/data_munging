"""Contains class for reading data from a football league table stored
as a text file."""
from extract_analyse import DataExtractor

class FootballTableExtractor(DataExtractor):
    """Inherits from DataExtractor and redefines read_rows() to adapt to
    a football league table."""
    def read_rows(self):
        """Reads rows in a football league table data file."""
        self.file.seek(0)
        self.file.readline()
        self.row_list = []
        for each_team in self.file.readlines():
            each_team_data = each_team.split()
            if len(each_team_data) == 0:
                continue
            try:
                each_team_data.remove('-')
            except ValueError:
                pass

            if each_team_data[0][:-1].isdigit(): #Ignore rows without a number
                                                #as first element.
                each_team_dict = dict(zip(self.columns, each_team_data[1:]))
                self.row_list.append(each_team_dict)
